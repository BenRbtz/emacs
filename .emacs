;;====================================================================================
;; Remove whiteplace before saving
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;;====================================================================================
;; Debugging mode
;; (setq debug-on-error t)

;;====================================================================================
;; Melpa
(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (url (concat (if no-ssl "http" "https") "://melpa.org/packages/")))
  (add-to-list 'package-archives (cons "melpa" url) t))
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/")))
(package-initialize)

;;====================================================================================
;;; Flychecker

;; Note:
;  - Comment-out custom variable and ignore the next two points if you don't want standard-checks
;  - Ensure PSR2Revised.xml file is in the right place
;  - Ensure phpcs is installed on the system,

;; flymake-phpcs

;; (custom-set-variables
;;  ;; custom-set-variables was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(flycheck-phpcs-standard "PSR2")
;;  '(package-selected-packages
;;    (quote
;;     (smartparens python-mode flymake-phpcs flymake-php flycheck ac-php))))

(custom-set-variables '(flycheck-phpcs-standard "/PSR2Revised.xml"))

;; Include Flycheck mode in another mode
(add-hook 'after-init-hook #'global-flycheck-mode)

;;====================================================================================
;;Smart parenthsis
(require 'smartparens-config)
(add-hook 'php-mode-hook #'smartparens-mode)

;;====================================================================================
;; Backup temp files in $TMPDIR/emacs$UID/
(defconst emacs-tmp-dir (expand-file-name (format "emacsBackFiles/emacs%d" (user-uid)) temporary-file-directory))
(setq backup-directory-alist
      `((".*" . ,emacs-tmp-dir)))
(setq auto-save-file-name-transforms
      `((".*" ,emacs-tmp-dir t)))
(setq auto-save-list-file-prefix
      emacs-tmp-dir)

;;====================================================================================
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
